import 'package:json_annotation/json_annotation.dart';

part 'list_issues_model.g.dart';

@JsonSerializable()
class ListIssuesModel {
  final List<dynamic> results;
  final int number_of_page_results, offset, limit, number_of_total_results;
  final String error;

  ListIssuesModel(
      {required this.results,
      required this.number_of_page_results,
        required this.error,
        required this.limit,
        required this.number_of_total_results,
      required this.offset});

  Map<String, dynamic> toJson() => _$ListIssuesModelToJson(this);

  factory ListIssuesModel.fromJson(Map<String, dynamic> json) =>
      _$ListIssuesModelFromJson(json);
}
