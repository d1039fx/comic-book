import 'package:json_annotation/json_annotation.dart';

part 'issues_model.g.dart';

@JsonSerializable()
class IssuesModel {
  final String? issue_number, name, api_detail_url, description;
  final Map<String, dynamic>? volume, image;
  final int? id;

  IssuesModel(
      {this.issue_number,
      this.id,
      this.name,
      this.api_detail_url,
      this.image,
      this.volume,
      this.description});

  Map<String, dynamic> toJson() => _$IssuesModelToJson(this);

  factory IssuesModel.fromJson(Map<String, dynamic> json) =>
      _$IssuesModelFromJson(json);
}
