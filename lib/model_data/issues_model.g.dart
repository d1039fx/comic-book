// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'issues_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IssuesModel _$IssuesModelFromJson(Map<String, dynamic> json) => IssuesModel(
      issue_number: json['issue_number'] as String?,
      id: json['id'] as int?,
      name: json['name'] as String?,
      api_detail_url: json['api_detail_url'] as String?,
      image: json['image'] as Map<String, dynamic>?,
      volume: json['volume'] as Map<String, dynamic>?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$IssuesModelToJson(IssuesModel instance) =>
    <String, dynamic>{
      'issue_number': instance.issue_number,
      'name': instance.name,
      'api_detail_url': instance.api_detail_url,
      'description': instance.description,
      'volume': instance.volume,
      'image': instance.image,
      'id': instance.id,
    };
