// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_issues_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListIssuesModel _$ListIssuesModelFromJson(Map<String, dynamic> json) =>
    ListIssuesModel(
      results: json['results'] as List<dynamic>,
      number_of_page_results: json['number_of_page_results'] as int,
      error: json['error'] as String,
      limit: json['limit'] as int,
      number_of_total_results: json['number_of_total_results'] as int,
      offset: json['offset'] as int,
    );

Map<String, dynamic> _$ListIssuesModelToJson(ListIssuesModel instance) =>
    <String, dynamic>{
      'results': instance.results,
      'number_of_page_results': instance.number_of_page_results,
      'offset': instance.offset,
      'limit': instance.limit,
      'number_of_total_results': instance.number_of_total_results,
      'error': instance.error,
    };
