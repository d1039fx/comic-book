import 'package:comic_book/list_comic_behavior/index_bloc.dart';

import 'package:comic_book/widget/list_issues.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'database/database_bloc.dart';

void main() {
  runApp(const ComicBookBloc());
}

class ComicBookBloc extends StatelessWidget {
  const ComicBookBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<DatabaseBloc>(create: (_) => DatabaseBloc()),
      BlocProvider<IndexBloc>(create: (_) => IndexBloc()),
    ], child: const ComicBook());
  }
}

class ComicBook extends StatelessWidget {
  const ComicBook({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Comic Book',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Comic Book'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController? _controller;
  AnimationController? _controllerInverted;
  Animation<double>? _animation;
  Animation<double>? _animationInverted;

  ScrollController listIssuesBehavior = ScrollController();

  bool stateAnimation = false;
  double range = 0;
  int indexPrevious = 0;
  int index = 0;
  List<int> rangePages = [0, 5];

  @override
  void initState() {
    // TODO: implement initState
    _controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controller!.reset();
        }
      });

    _controllerInverted = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controllerInverted!.reset();
        }
      });

    _animation = Tween<double>(
      begin: 0,
      end: 1.6,
    ).animate(_controller!);

    _animationInverted =
        Tween<double>(begin: 1.6, end: 0).animate(_controllerInverted!);

    _controller!.addListener(() {
      setState(() {
        range = _animation!.value;
      });
    });

    _controllerInverted!.addListener(() {
      setState(() {
        range = _animationInverted!.value;
      });
    });

    BlocProvider.of<IndexBloc>(context)
        .add(IndexData(indexListComic: rangePages));

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller!.dispose();
    _controllerInverted!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: BlocBuilder<IndexBloc, IndexState>(
          builder: (context, state) {
            indexPrevious = index + 1 == state.listindex.length
                ? state.listindex.length - 1
                : index + 1;

            int indexBottomPage = state.offsetList.length != 5
                ? indexPrevious
                : state.listindex[index] + 1 == state.listindex.length
                    ? state.listindex.length - 1
                    : state.listindex[index] + 1;

            return state.offsetList.length != 5
                ? state.offsetList.isEmpty
                    ? const Text('Iniciando Paginas...')
                    : Stack(
                        alignment: Alignment.center,
                        children: [
                          Text(
                            '${state.offsetList.length}/5',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          CircularProgressIndicator(
                            value: state.offsetList.length / 5,
                          ),
                        ],
                      )
                : Column(
                    children: [
                      Expanded(
                        child: Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            ListIssues(
                                //page: state.listindex[index],
                                page: _controllerInverted!.isAnimating
                                    ? state.listindex[index] + 1
                                    : indexBottomPage + 1,
                                listIssues: state.offsetList[
                                    _controllerInverted!.isAnimating
                                        ? index
                                        : indexPrevious]),
                            LayoutBuilder(builder: (context, size) {
                              return Transform(
                                transform: Matrix4.identity()
                                  ..setEntry(3, 2, 0.0003)
                                  ..rotateY(range),
                                child: GestureDetector(
                                  onHorizontalDragUpdate: (detail) {
                                    if (detail.delta.direction != 0) {
                                      if (index != state.listindex.length - 1) {
                                        _controller!
                                            .forward()
                                            .then((value) {})
                                            .whenComplete(() {
                                          if (index ==
                                              state.listindex.length - 1) {
                                            index = state.listindex.length - 1;
                                          } else {
                                            _controller!.reset();
                                            index++;
                                          }
                                        });
                                      }
                                    } else {
                                      if (index != 0) {
                                        _controllerInverted!
                                            .forward()
                                            .then((value) {})
                                            .whenComplete(() {
                                          _controller!.reset();
                                          if (index == 0) {
                                            index = 0;
                                          } else {
                                            index--;
                                          }
                                        });
                                      }
                                    }
                                  },
                                  child: ListIssues(
                                    //page: state.listindex[index],
                                    page: _controllerInverted!.isAnimating
                                        ? state.listindex[index]
                                        : state.listindex[index] + 1,
                                    listIssues: state.offsetList[
                                        _controllerInverted!.isAnimating
                                            ? index - 1
                                            : index],
                                  ),
                                ),
                              );
                            })
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                //solo crea la lista
                                if (rangePages[0] != 0) {
                                  int startRange = rangePages[0] - 5;
                                  int endRange = rangePages[1] - 5;
                                  rangePages[0] = startRange;
                                  rangePages[1] = endRange;

                                  BlocProvider.of<IndexBloc>(context).add(
                                      IndexData(indexListComic: rangePages));
                                }
                              },
                              child: const Text('<<')),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: state.listindex
                                  .map<InkWell>((e) => InkWell(
                                      onTap: () {
                                        int pos = state.listindex.indexWhere(
                                            (element) => element == e);

                                        setState(() {
                                          index = pos;
                                        });
                                      },
                                      child: e == state.listindex[index]
                                          ? CircleAvatar(
                                              radius: 15,
                                              child: Text(
                                                '${e + 1}',
                                                style: const TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            )
                                          : Text(
                                              '${e + 1}',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )))
                                  .toList(),
                            ),
                          )),
                          ElevatedButton(
                              onPressed: () {
                                //solo crea la lista
                                int startRange = rangePages[0] + 5;
                                int endRange = rangePages[1] + 5;
                                rangePages[0] = startRange;
                                rangePages[1] = endRange;

                                BlocProvider.of<IndexBloc>(context)
                                    .add(IndexData(indexListComic: rangePages));
                              },
                              child: const Text('>>'))
                        ],
                      )
                    ],
                  );
          },
        ),
      ),
    );
  }
}
