part of 'index_bloc.dart';

@immutable
abstract class IndexEvent {
  final List<int> indexListComic;

  const IndexEvent(this.indexListComic);
}

class IndexData extends IndexEvent {
  const IndexData({required List<int> indexListComic}) : super(indexListComic);
}
