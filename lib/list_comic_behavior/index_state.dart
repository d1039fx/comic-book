part of 'index_bloc.dart';

@immutable
abstract class IndexState {
  final List<int> listindex;
  final List<List<IssuesModel>> offsetList;

  const IndexState({required this.listindex, required this.offsetList});
}

class IndexInitial extends IndexState {
  const IndexInitial({required List<int> listIndex, required List<List<IssuesModel>> offsetList})
      : super(listindex: listIndex, offsetList: offsetList);
}

class CantPages extends IndexState{
   CantPages(int lenght) : super(listindex: [], offsetList: []);
}
