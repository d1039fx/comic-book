import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:comic_book/class/data_base.dart';
import 'package:comic_book/model_data/issues_model.dart';
import 'package:comic_book/model_data/list_issues_model.dart';

import 'package:meta/meta.dart';

part 'index_event.dart';
part 'index_state.dart';

class IndexBloc extends Bloc<IndexEvent, IndexState> {
  IndexBloc() : super(const IndexInitial(listIndex: [], offsetList: [])) {
    on<IndexEvent>((event, emit) async {
      List<int> listIndex = [];
      List<int> listParcial = [];
      List<List<IssuesModel>> offsetList = [];
      int cantResult = 20;

      await reponseServerIssues(offset: '0', limit: cantResult.toString()).then((value) {
        listParcial.clear();
        int totalResult =
            (value.number_of_total_results / cantResult).ceilToDouble().toInt();
        for (int i = 0; i < totalResult; i++) {
          listIndex.add(i);
        }
      }).whenComplete(() {
        listParcial =
            listIndex.sublist(event.indexListComic[0], event.indexListComic[1]);
      });

      for (int indexData in listParcial) {
        offsetList.add(
            await reponseServerIssues(offset: (indexData * cantResult).toString(), limit: cantResult.toString())
                .then((value) => value.results.map<IssuesModel>((e) {
              return IssuesModel.fromJson(e);
            }).toList()).whenComplete((){

              emit(IndexInitial(listIndex: listParcial, offsetList: offsetList));
            }));
      }

      //emit(IndexInitial(listIndex: listParcial, offsetList: offsetList));

    });
  }
  Future<ListIssuesModel> reponseServerIssues({required String offset, required String limit}) async =>
      await DataBase().readDataIssues(offset, limit).then((value) {
        //print('status index: ${value.statusCode}');
        Map<String, dynamic> data = jsonDecode(value.body);
        return ListIssuesModel.fromJson(data);
      }).catchError((error){
        print(error);
      });
}
