import 'package:comic_book/model_data/issues_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

class ListIssues extends StatelessWidget {
  ListIssues({Key? key, required this.listIssues, required this.page})
      : super(key: key);

  final List<IssuesModel> listIssues;
  final int page;
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]!),
          borderRadius: const BorderRadius.only(
              topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
          color: Colors.white),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Pagina #$page',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: ListView.separated(
                controller: _scrollController,
                itemBuilder: (_, index) {
                  IssuesModel issuesModel = listIssues[index];
                  return ListTile(
                    leading: FadeInImage.assetNetwork(
                        placeholder: 'assets/images/comic_vine.png',
                        image: kIsWeb
                            ? 'https://cors-anywhere.herokuapp.com/${issuesModel.image!['icon_url']!}'
                            : issuesModel.image!['icon_url']!),
                    title: Text(issuesModel.name ?? 'Sin nombre'),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(issuesModel.volume!['name']),
                        HtmlWidget(
                            issuesModel.description ?? 'Sin descripcion'),
                      ],
                    ),
                  );
                },
                separatorBuilder: (_, index) => const Divider(),
                itemCount: listIssues.length),
          ),
        ],
      ),
    );
    // return BlocBuilder<DatabaseBloc, DatabaseState>(
    //   builder: (context, state) {
    //     return ListView.separated(
    //         controller: _scrollController,
    //         itemBuilder: (_, index) {
    //           IssuesModel issuesModel = state.responseBody[index];
    //           return ListTile(
    //             leading: FadeInImage.assetNetwork(
    //                 placeholder: 'assets/images/comic_vine.png',
    //                 image: kIsWeb
    //                     ? 'https://cors-anywhere.herokuapp.com/${issuesModel.image!['icon_url']!}'
    //                     : issuesModel.image!['icon_url']!),
    //             title: Text(issuesModel.name ?? ''),
    //             subtitle: Text(index.toString()),
    //           );
    //         },
    //         separatorBuilder: (_, index) => const Divider(),
    //         itemCount: state.responseBody.length);
    //   },
    // );
  }
}
