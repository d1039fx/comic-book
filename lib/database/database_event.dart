part of 'database_bloc.dart';

@immutable
abstract class DatabaseEvent {
  final List<IssuesModel> offset;
  final int page;

  const DatabaseEvent(this.offset, this.page);
}

class GetResponseDataBase extends DatabaseEvent{
  const GetResponseDataBase({required List<IssuesModel> offset, required int page}) : super(offset, page);
}

