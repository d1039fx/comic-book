import 'package:bloc/bloc.dart';
import 'package:comic_book/model_data/issues_model.dart';

import 'package:meta/meta.dart';

part 'database_event.dart';
part 'database_state.dart';

class DatabaseBloc extends Bloc<DatabaseEvent, DatabaseState> {
  DatabaseBloc() : super(const DatabaseInitial(responseBody: [], page: 0)) {
    on<DatabaseEvent>((event, emit) async {
      emit(DatabaseInitial(responseBody: event.offset, page: event.page));
    });
  }
}
