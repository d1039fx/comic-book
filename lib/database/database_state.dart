part of 'database_bloc.dart';

@immutable
abstract class DatabaseState {
  final List<IssuesModel> responseBody;
  final int page;

  const DatabaseState(this.responseBody, this.page);
}

class DatabaseInitial extends DatabaseState {
  const DatabaseInitial({required List<IssuesModel> responseBody, required int page})
      : super(responseBody, page);
}
