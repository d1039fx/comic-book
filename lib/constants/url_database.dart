import 'package:flutter/foundation.dart';

class UrlDataBase {
  final String token = 'fd34920b856edcecbf63b6a33a73099c3e0fdca4';
  final String urlApi =
      kIsWeb ? 'cors-anywhere.herokuapp.com' : 'comicvine.gamespot.com';
  final String pathIssue =
      kIsWeb ? 'https://www.comicvine.gamespot.com/api/issue' : 'api/issue';
  final String pathIssues =
      kIsWeb ? 'https://www.comicvine.gamespot.com/api/issues' : '/api/issues';

  Uri urlIssues(String offset, String limit) => Uri.https(urlApi, pathIssues, {
        'api_key': token,
        'format': 'json',
        'sort': 'id:desc',
        'field_list':
            'issue_number,id,name,api_detail_url,description,volume,image',
        'limit': limit,
        'offset': offset
      });

  Uri urlIssue({required String idIssue}) =>
      Uri.https(urlApi, '$pathIssue/4000-$idIssue/', {
        'api_key': token,
        'format': 'json',
        'field_list': 'name,description,image',
      });
}
