import 'dart:io';

import 'package:comic_book/constants/url_database.dart';
import 'package:http/http.dart' as http;

class DataBase with UrlDataBase {
  Future<http.Response> readDataIssues(String offset, String limit) async {
    //print(urlIssues(offset, limit));
    return await http
        .get(urlIssues(offset, limit),headers: {
      "Access-Control_Allow_Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT",
      "Access-Control-Allow-Headers": "Content-Type"
    })
        .then((value) => value)
        .catchError((error) {
      print(error);
    });
  }

  // Future<http.Response> readDataIssue({required idIssue}) async {
  //   return await http
  //       .get(urlIssue(idIssue: idIssue), headers: {
  //         "Accept": "application/json",
  //         "Access-Control_Allow_Origin": "*"
  //       })
  //       .then((value) => value)
  //       .catchError((error) {
  //         print(error.toString());
  //       });
  // }
}
